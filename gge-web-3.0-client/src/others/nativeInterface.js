import store from "../store";
import { hideClassChooser, hideOtherView } from "../action/otherActions";
import { fetchSubstitutionsStart, fetchSubstitutions } from "../action/substitutionActions";
import { getString } from "./language";

export function isAvailable() {
    return Boolean(window.GGENative);
}

export function getVersion() {
    if(isAvailable()) {
        return window.GGENative.getVersion();
    } else {
        return null;
    }
}

export function setBarColor(color) {
    if(isAvailable()) {
        window.GGENative.setBarColor(color);
    }
}

export function registerBackListener() {
    if(isAvailable()) {
        window.addEventListener("keypress", e => {
            if(e.keyCode === 92) {
                if(!goBack()) {
                    exitApplication();
                }
            }
        });
    }
}

function goBack() {
    var state = store.getState();

    if(state.other.classSelectorState.open) {
        store.dispatch(hideClassChooser());
        return true;
    }

    if(state.other.view !== "substitutions") {
        store.dispatch(fetchSubstitutionsStart());
        store.dispatch(hideOtherView());
        store.dispatch(fetchSubstitutions());
        return true;
    }

    return false;
}

export function exitApplication() {
    if(isAvailable()) {
        window.GGENative.exitApplication();
    }
}

export function canNativeShare() {
    if(isAvailable()) {
        return window.GGENative.canShare && window.GGENative.canShare();
    }
}

export function nativeShare(data, text) {
    if(isAvailable() && window.GGENative.share) {
        window.GGENative.share(data, text);
    }
}

export function getAppName() {
    if(isAvailable()) {
        if(window.GGENative.getAppName) {
            return window.GGENative.getAppName();
        } else {
            return getString("app.name");
        }
    } else {
        return getString("app.name");
    }
}