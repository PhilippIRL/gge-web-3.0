import { getString } from "./language";
import store from "../store";

export function writeCache(substitutions) {
    var cacheObj = {substitutions, cacheTime: Date.now(), classes: store.getState().settings.enabledClasses};
    var json = JSON.stringify(cacheObj);
    window.localStorage.ggeWebCache = json;
}

export function readCache() {
    var json = window.localStorage.ggeWebCache;
    var maxOffset = 1000 * 60 * 60 * 24 * 2;
    if(!json || json.cacheTime < Date.now() - maxOffset) {
        return null;
    } else {
        var cache = JSON.parse(json);
        var date = new Date(cache.cacheTime);
        var formatedDate =  formatDate(date);
        var substitutions = cache.substitutions;
        var oldEnabledClasses = cache.classes;
        var newEnabledClasses = store.getState().settings.enabledClasses;
        if(oldEnabledClasses.sort().join(",") !== newEnabledClasses.sort().join(",")) {
            substitutions.unshift({
                type: "custom",
                title: getString("tips.changedClasslistOffline.title"),
                text: getString("tips.changedClasslistOffline.text"),
                date: getString("tips.name"),
            });
        }
        substitutions.unshift({
            type: "custom",
            title: getString("tips.cachedData.title").replace("{0}", formatedDate),
            text: getString("tips.cachedData.text").replace("{0}", formatedDate),
            date: getString("tips.name"),
        });
        return substitutions;
    }
}

export function writeClassListCache(classList) {
    var cacheObj = {classList};
    var json = JSON.stringify(cacheObj);
    window.localStorage.ggeWebClassCache = json;
}

export function readClassListCache() {
    var json = window.localStorage.ggeWebClassCache;
    return json ? JSON.parse(json) : null;
}

function formatNumber(number, minLength) {
    var str = number.toString();
    while(str.length < minLength) {
        str = "0" + str;
    }
    return str;
}

function formatDate(date) {
    return `${date.getDate()}.${date.getMonth()+1}.${date.getFullYear()} - ${formatNumber(date.getHours(), 2)}:${formatNumber(date.getMinutes(), 2)}`;
}