async function sha512(str) {
    if(!window.crypto || !window.crypto.subtle) {
        var sha512 = await import("./sha512");
        console.log(sha512)
        return sha512.default(str);
    }
    return await crypto.subtle.digest("SHA-512", new TextEncoder("utf-8").encode(str)).then(buf => {
        return Array.prototype.map.call(new Uint8Array(buf), x=>(('00'+x.toString(16)).slice(-2))).join('');
    });
}

export async function genCredentailToken(username, password) {
    return (await sha512(JSON.stringify({username, password}))).substr(0, 40);
}