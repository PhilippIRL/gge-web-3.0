var installEvent = null;

export function registerInstallListeners() {
    window.addEventListener('beforeinstallprompt', (e) => {
        installEvent = e;
    });
}

function checkIfiOSInstallable() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent);
    if(iOS) {
        return window.navigator.standalone === false;
    }
}

export function getInstallable() {
    if(installEvent) {
        return {prompt: () => {
            installEvent.prompt();
            installEvent.userChoice.then(result => {
                if(result.outcome === "accepted") {
                    window.location.reload();
                }
            });
        }, type: "DEFAULT"};
    } else if(checkIfiOSInstallable()) {
        return {type: "APPLE"};
    } else {
        return {type: "NONE"};
    }
}