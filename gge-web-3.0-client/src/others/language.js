import en from "./langs/en";
import de from "./langs/de";

export const language = navigator.language.split("-")[0];

setDocumentLanguage(language);

var languages = {};
languages.en = en;
languages.de = de;

export function getString(id) {
    if(languages[language] && languages[language][id]) {
        return languages[language][id];
    } else if(languages["en"] && languages["en"][id]) {
        return languages["en"][id];
    } else {
        return id;
    }
}

function setDocumentLanguage(lang) {
    var elem = document.querySelector("html[lang]");
    elem.setAttribute("lang",lang);
}