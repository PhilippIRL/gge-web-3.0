import { getString } from "./language";
import shareTemplateFile from "../assets/gge_share.png";
import { canNativeShare, nativeShare } from "./nativeInterface";

var nativeShareAvailable = canNativeShare();

export function share(substitution) {
    polyfillOffscreenCanvas();

    var shareTemplate = document.createElement("img");
    shareTemplate.src = shareTemplateFile;
    shareTemplate.onload = function() {
        var canvas = new OffscreenCanvas(512, 512);

        var ctx = canvas.getContext("2d");
        ctx.drawImage(shareTemplate, 0, 0);

        var title;
        var text;
        var lineHeight = 25;

        if(substitution.type === "motd") {
            title = getString("entries.motd").replace("{0}", substitution.date);
            text = substitution.texts.join("\n\n");
        } else if(substitution.type === "substitution") {
            lineHeight = 30;
            title = getString("share.substitution.title");
            text = getString("entries.class").replace("{0}", substitution.class_) + "\n" +
                getString("entries.date").replace("{0}", substitution.date) + "\n" +
                getString("entries.lesson").replace("{0}", substitution.lesson) + "\n" +
                getString("entries.newRoom").replace("{0}", substitution.newroom) + "\n" +
                getString("entries.oldRoom").replace("{0}", substitution.oldroom) + "\n" +
                getString("entries.subject").replace("{0}", substitution.subject) + "\n" +
                getString("entries.substitute").replace("{0}", substitution.substitute) + "\n" +
                getString("entries.text").replace("{0}", substitution.text);
        } else {
            title = "Unknown entry";
            text = "Invalid data";
        }

        ctx.fillStyle = "#fff";
        ctx.font = "24px " + selectFont();
        ctx.textAlign = "center";

        ctx.fillText(title, 512/2, 50);

        ctx.textAlign = "left";    
        wrapText(ctx, text, ((512-450)/2), 100, 450, lineHeight);

        canvas.convertToBlob({type: "image/png"})
        .then(blob => { 
            if(nativeShareAvailable) {
                var reader = new FileReader();
                reader.onload = e => nativeShare(e.target.result, "");
                reader.readAsDataURL(blob);
            } else {
                if(navigator.canShare) {
                    var file = new File([blob], "gge-share.png", {type: blob.type});
                    navigator.share({
                        files: [file],
                    });
                } else {
                    var aTag = document.createElement("a");
                    aTag.setAttribute("download", "gge-vertretung-share.png");
                    aTag.href = URL.createObjectURL(blob);
                    aTag.click();
                }
            }
        });
    }
}

export function sharingAvailable() {
    return nativeShareAvailable || Boolean(navigator.canShare);
}

/* https://stackoverflow.com/a/11361958 */
function wrapText(context, text, x, y, maxWidth, lineHeight) {
    var cars = text.split("\n");

    for (var ii = 0; ii < cars.length; ii++) {

        var line = "";
        var words = cars[ii].split(" ");

        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + " ";
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;

            if (testWidth > maxWidth) {
                context.fillText(line, x, y);
                line = words[n] + " ";
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }

        context.fillText(line, x, y);
        y += lineHeight;
    }
 }

 /* https://gist.github.com/n1ru4l/9c7eff52fe084d67ff15ae6b0af5f171 */
 function polyfillOffscreenCanvas() {
    if (!window.OffscreenCanvas) {
        window.OffscreenCanvas = class OffscreenCanvas {
          constructor(width, height) {
            this.canvas = document.createElement("canvas");
            this.canvas.width = width;
            this.canvas.height = height;
      
            this.canvas.convertToBlob = () => {
              return new Promise(resolve => {
                this.canvas.toBlob(resolve);
              });
            };
      
            return this.canvas;
          }
        };
      }
 }

 function selectFont() {
     var fontList = [
        "Roboto",
        "Arial",
        "Sans",
        "Open Sans",
        "-apple-system",
     ];
     for(var i = 0; i < fontList.length; i++) {
         var font = fontList[i];
         if(document.fonts.check("24px " + font)) {
            return font;
         }
     }
     return "sans-serif";
 }