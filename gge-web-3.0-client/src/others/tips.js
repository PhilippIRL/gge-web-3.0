import store from "../store";
import { showSettings, showClassChooser } from "../action/otherActions";
import { getString } from "./language";
import { getInstallable } from "./pwa";

export function addTips(substitutions) {

    var installable = getInstallable();
    if(installable.type === "APPLE") {
        substitutions.unshift({
            type: "custom",
            title: getString("tips.installIOS.title"),
            text: getString("tips.installIOS.text"),
            date: getString("tips.name"),
        });
    } else if(installable.type === "DEFAULT") {
        substitutions.unshift({
            type: "custom",
            title: getString("tips.installGeneric.title"),
            text: getString("tips.installGeneric.text"),
            date: getString("tips.name"),
            clickAction: () => {
                installable.prompt();
            },
            buttonText: getString("tips.installGeneric.buttonText"),
        });
    }
    
    if(substitutions.length === 0) {
        substitutions.unshift({
            type: "custom",
            title: getString("tips.noEntries.title"),
            text: getString("tips.noEntries.text"),
            date: getString("tips.name"),
        });
    } else if(store.getState().settings.enabledClasses.length === 0) {
        substitutions.unshift({
            type: "custom",
            title: getString("tips.enableClasses.title"),
            text: getString("tips.enableClasses.text"),
            date: getString("tips.name"),
            clickAction: () => {
                store.dispatch(showSettings());
                store.dispatch(showClassChooser());
            },
            buttonText: getString("tips.enableClasses.launchClassChooser"),
        });
    }

    return substitutions;
}