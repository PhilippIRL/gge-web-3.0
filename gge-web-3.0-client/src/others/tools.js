import { sharingAvailable } from "./share";
import { API_BASE, APP_VERSION } from "../config";
import { getVersion } from "./nativeInterface";

export function isMobile() {
    return Boolean(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
}

export function getSharingMode() {
    var sharingSupport = sharingAvailable();
    var mobile = isMobile();
    if(sharingSupport) {
        return "SHARE";
    } else if(!mobile) {
        return "DOWNLOAD";
    } else {
        return "DISABLE";
    }
}

export function getVersions() {
    return {app: APP_VERSION, native: getVersion()};
}

export function showDebugDialog() {
    var { app, native } = getVersions();
    var debugText = `App Version: ${app}\nNative Version: ${native}\nEnvironment: ${process.env.NODE_ENV}\nAPI BASE: ${API_BASE}\nOrigin: ${window.origin}\nUser-agent: ${window.navigator.userAgent}`;
    window.alert(debugText);
}