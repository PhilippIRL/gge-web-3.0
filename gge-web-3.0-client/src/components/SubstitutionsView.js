import React from 'react';
import "../styles/SubstitutionsView.css";
import SubstitutionView from "./SubstitutionView";

export default class SubstitutionsView extends React.Component {
    render() {
      var substitutionViews = this.props.substitutions.map((substitution, index) => {
        if(substitution.show) {
          return <SubstitutionView key={index} index={index} data={substitution} dispatch={this.props.dispatch} />
        } else {
          return null;
        }
      })
      .filter(substitution => substitution !== null);
      return (
        <div className="substitutionsView">
            <div className="innerSubstitutionsView">
                {substitutionViews}
            </div>
        </div>
      );
    }
  }