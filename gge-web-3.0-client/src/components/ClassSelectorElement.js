import React from "react";
import { saveSettings } from "../action/settingsActions";
import { addClass, removeClass } from "../action/settingsActions";
import "../styles/ClassSelectorElement.css";

export default class ClassSelectorElement extends React.Component {

    constructor(props) {
        super(props);

        this.toggleOwnClass = this.toggleOwnClass.bind(this);
    }

    toggleOwnClass(e) {
        var enable = !this.props.enabled;
        if(enable) {
            this.props.dispatch(addClass(this.props.class_));
        } else {
            this.props.dispatch(removeClass(this.props.class_));
        }
        this.props.dispatch(saveSettings());
    }

    render() {
        return (
            <div className="classSelectorElement" onClick={this.toggleOwnClass}>
                <input type="checkbox" className="classSelectorCheckbox" checked={this.props.enabled} readOnly />
                <span className="classSelectorLabel">{this.props.class_}</span>
            </div>
        );
    }
}