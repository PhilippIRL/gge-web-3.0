import React from "react";
import "../styles/ErrorView.css";
import Button from "./Button";
import { fetchSubstitutionsStart, fetchSubstitutions } from "../action/substitutionActions";
import { getString } from "../others/language";

export default class ErrorView extends React.Component {

    constructor(props) {
        super(props);
        this.retry = this.retry.bind(this);
    }

    retry() {
        this.props.dispatch(fetchSubstitutionsStart());
        this.props.dispatch(fetchSubstitutions());
    }

    render() {
        var text = this.props.error || getString("loading.text");
        var icon = this.props.error ? "sync_problem" : "sync";
        var retryButton;
        if(this.props.error) {
            retryButton = <Button text={getString("loading.retry")} onClick={this.retry} />;
        }
        return (
            <div className="loadingScreen">
                <span className={this.props.error ? "material-icons loader error" : "material-icons loader loading"}>{icon}</span>
                <span className="loadingText">{text}</span>
                {retryButton}
            </div>
        );
    }
}