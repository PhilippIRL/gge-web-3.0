import React from "react";
import "../styles/Button.css";

export default class Button extends React.Component {
    render() {
        var className = this.props.passive ? "button passive" : "button";
        return (
            <div onClick={this.props.onClick} className={className}>
                <span className="buttonText">{this.props.text}</span>
            </div>
        );
    }
}