import React from "react";
import "../styles/TabbedSubstitutionsView.css";
import { setTab } from "../action/substitutionActions";
import SubstitutionsView from "./SubstitutionsView";

export default class TabbedSubstitutionsView extends React.Component {

    constructor(props) {
        super(props);

        this.switchTab = this.switchTab.bind(this);
    }

    switchTab(index) {
        return (function(e) {
            this.props.dispatch(setTab(index));
        }).bind(this);
    }
    
    render() {
        var tabs = [];
        this.props.substitutions.forEach(substitution => {
            if(!tabs.includes(substitution.data.date)) {
                tabs.push(substitution.data.date);
            }
        });
        var tabSelectors = tabs.map((tab, index) => {
            var className = "tabSelector";
            if(this.props.selectedTab === index) {
                className += " selectedTab";
            }
            return (
                <div key={tab} className={className} onClick={this.switchTab(index)}>
                    <span className="tabSelectorText">{tab}</span>
                </div>
            );
        });
        var substitutions = this.props.substitutions.map(substitution => {
            return {...substitution, show: substitution.data.date === tabs[this.props.selectedTab]};
        });
        return (
            <div className="tabbedSubstitutionView">
                <div className="tabSelectors">{tabSelectors}</div>
                <SubstitutionsView substitutions={substitutions} dispatch={this.props.dispatch} />
            </div>
        )
    }
}