import React, { Suspense } from 'react';
import Header from './Header';
import '../styles/App.css';
import { connect } from "react-redux";
import substitutionsActions from "../action/substitutionActions";
import ErrorView from "./ErrorView";
//import SettingsView from './SettingsView';
//import TabbedSubstitutionsView from './TabbedSubstitutionsView';
import { showLogin } from '../action/otherActions';
import { setBarColor } from '../others/nativeInterface';
//import Login from './Login';

const SettingsView = React.lazy(() => import("./SettingsView"));
const TabbedSubstitutionsView = React.lazy(() => import("./TabbedSubstitutionsView"));
const Login = React.lazy(() => import("./Login"));

class App extends React.Component {

  componentDidMount() {
    if(!this.props.settings.authToken) {
      this.props.dispatch(showLogin());
    }
    if(!this.props.loaded && !this.props.fetching) {
      this.props.dispatch(substitutionsActions.fetchSubstitutionsStart());
      this.props.dispatch(substitutionsActions.fetchSubstitutions());
    }
  }

  componentDidUpdate() {
    if(!this.props.settings.authToken && this.props.other.view !== "login") {
      this.props.dispatch(showLogin());
    }
  }

  render() {

    var contentView = null;
    if(this.props.other.view === "substitutions") {
      if(this.props.substitutions.loaded && !this.props.substitutions.error) {
        contentView = <TabbedSubstitutionsView substitutions={this.props.substitutions.substitutions} selectedTab={this.props.substitutions.tab} dispatch={this.props.dispatch} />;
      } else {
        contentView = <ErrorView error={this.props.substitutions.error} loaded={this.props.substitutions.loaded} fetching={this.props.substitutions.fetching} dispatch={this.props.dispatch} />;
      }
    } else if(this.props.other.view === "settings") {
      contentView = <SettingsView dispatch={this.props.dispatch} settings={this.props.settings} other={this.props.other} />;
    } else if(this.props.other.view === "login") {
      contentView = <Login dispatch={this.props.dispatch} other={this.props.other} />;
    }
    var className = "app";
    var themeMetaTag = document.querySelector("meta[name=\"theme-color\"]");
    if(this.props.settings.theme === "dark") {
      className += " darktheme";
      themeMetaTag.setAttribute("content","#000000")
      setBarColor("#000000");
    } else {
      themeMetaTag.setAttribute("content","#BB0000")
      setBarColor("#BB0000");
    }
    return (
      <div className={className}>
        <Header other={this.props.other} dispatch={this.props.dispatch} view={this.props.other.view} displaySettingsIcon={Boolean(!this.props.substitutions.error && this.props.substitutions.loaded)} />
        <Suspense fallback={<ErrorView loaded={false} fetching dispatch={this.props.dispatch} />}>
          {contentView}
        </Suspense>
      </div>
    );
  }

}

export default connect(store => store)(App);