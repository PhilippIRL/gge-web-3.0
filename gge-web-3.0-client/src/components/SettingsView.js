/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "../styles/SettingsView.css";
import { setTheme, saveSettings } from "../action/settingsActions";
import Button from "./Button";
import { showClassChooser } from "../action/otherActions";
import ClassSelector from "./ClassSelector";
import { getString } from "../others/language";
import { showDebugDialog } from "../others/tools";

export default class SettingsView extends React.Component {

    constructor(props) {
        super(props);

        this.darkModeChange = this.darkModeChange.bind(this);
        this.showClassSelector = this.showClassSelector.bind(this);
    }

    darkModeChange(e) {
        var darkmode = e.target.checked;
        this.props.dispatch(setTheme(darkmode ? "dark" : "light"));
        this.props.dispatch(saveSettings());
    }

    showClassSelector(e) {
        this.props.dispatch(showClassChooser());
    }

    render() {
        if(this.props.other.classSelectorState.open) {
            return (
                <div className="settingsView">
                    <ClassSelector other={this.props.other} dispatch={this.props.dispatch} settings={this.props.settings}/>
                </div>
            )
        } else {
            return (
                <div className="settingsView">
                    <div className="settingDarkMode">
                        <input className="darkModeCheckbox" type="checkbox" onChange={this.darkModeChange} checked={this.props.settings.theme === "dark"}></input>
                        <span className="darkModeLabel">{getString("settings.darkmode")}</span>
                    </div>
                    <div className="settingEnabledClasses">
                        <Button text={getString("settings.classSwitcher")} onClick={this.showClassSelector} />
                    </div>
                    <div className="license">
                        <span className="licenseText">
                            (C) Copyright PhilippIRL 2020
                            <br/><br/>
                            This program is free software: you can redistribute it and/or modify
                            it under the terms of the GNU Affero General Public License as published
                            by the Free Software Foundation, either version 3 of the License, or
                            (at your option) any later version.
                            This program is distributed in the hope that it will be useful,
                            but WITHOUT ANY WARRANTY; without even the implied warranty of
                            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                            GNU Affero General Public License for more details.
                            You should have received a copy of the GNU Affero General Public License
                            along with this program.
                            If not, see <a href="https://www.gnu.org/licenses/" rel="noopener noreferrer" target="_blank">http://www.gnu.org/licenses/</a>.
                            <br/><br/> 
                            <a href="https://t.me/ggevertretung" rel="noopener noreferrer" target="_blank">Telegram-Channel</a>
                            <br/>
                            <a href="https://gitlab.com/PhilippIRL/gge-web-3.0/blob/master/OPENSOURCE_LICENSES.md" rel="noopener noreferrer" target="_blank">Open Source-Licenses</a>
                            <br/> 
                            <a href="https://gitlab.com/PhilippIRL/gge-web-3.0" rel="noopener noreferrer" target="_blank">Source Code</a>
                            <br/>
                            <a href="https://null" onClick={e => {e.preventDefault();showDebugDialog()}}>Show Debug Dialog</a>
                        </span>
                    </div>
                </div>
            );
        }
    }
}