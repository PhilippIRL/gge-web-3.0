import React from "react";
import "../styles/Substitution.css";
import { toggleExpanded } from "../action/substitutionActions";
import Button from "./Button";
import { getString } from "../others/language";
import { findDOMNode } from "react-dom";
import { share, sharingAvailable } from "../others/share";
import { getSharingMode } from "../others/tools";

export default class SubstitutionView extends React.Component {

    constructor(props) {
        super(props);
        this.expandToggle = this.expandToggle.bind(this);
        this.substitutionButtonClick = this.substitutionButtonClick.bind(this);
    }

    expandToggle() {
        this.props.dispatch(toggleExpanded(this.props.index));
    }

    substitutionButtonClick(e) {
        e.stopPropagation();
        var substitution = this.props.data.data;
        if(substitution.type === "custom" && substitution.clickAction) {
            substitution.clickAction(e);
        } else {
            var sharingMode = getSharingMode();
            switch(sharingMode) {
                case "SHARE":
                case "DOWNLOAD":
                    share(substitution);
                    break;
                case "DISABLE":
                default:
                    alert(getString("share.disabled.description"));
                    break;
            }
        }
    }

    componentDidMount() {
        var mountedElement = findDOMNode(this);
        if(mountedElement.classList.contains("expanded")) {
            mountedElement.style.maxHeight = mountedElement.scrollHeight + "px";
            mountedElement.style.transition = "none";
        } else {
            mountedElement.style.maxHeight = "";
            mountedElement.style.transition = "";
        }
    }

    componentDidUpdate() {
        var mountedElement = findDOMNode(this);
        if(mountedElement.classList.contains("expanded")) {
            mountedElement.style.maxHeight = mountedElement.scrollHeight + "px";
            mountedElement.style.transition = "";
        } else {
            mountedElement.style.maxHeight = "";
            mountedElement.style.transition = "";
        }
    }

    render() {
        var substitution = this.props.data.data;
        var title = "";
        var details = [];

        var sharingMode = getSharingMode();
        var buttonText = sharingAvailable() ? getString("entries.share") : getString("entries.download");
        switch(sharingMode) {
            case "SHARE":
                buttonText = getString("entries.share");
                break;
            case "DOWNLOAD":
                buttonText = getString("entries.download");
                break;
            case "DISABLE":
            default:
                buttonText = getString("share.disabled");
                break;
        }

        var hideButton = false;
        var buttonClick = this.substitutionButtonClick;
        if(substitution.type === "substitution") {
            title = substitution.subject + " | " + substitution.lesson + " | " + substitution.substitute;
            details.push(<span className="detail" key={0}>{getString("entries.class").replace("{0}", substitution.class_)}</span>);
            details.push(<span className="detail" key={1}>{getString("entries.date").replace("{0}", substitution.date)}</span>);
            details.push(<span className="detail" key={2}>{getString("entries.lesson").replace("{0}", substitution.lesson)}</span>);
            details.push(<span className="detail" key={3}>{getString("entries.newRoom").replace("{0}", substitution.newroom)}</span>);
            details.push(<span className="detail" key={4}>{getString("entries.oldRoom").replace("{0}", substitution.oldroom)}</span>);
            details.push(<span className="detail" key={5}>{getString("entries.subject").replace("{0}", substitution.subject)}</span>);
            details.push(<span className="detail" key={6}>{getString("entries.substitute").replace("{0}", substitution.substitute)}</span>);
            details.push(<span className="detail" key={7}>{getString("entries.text").replace("{0}", substitution.text)}</span>);
        } else if(substitution.type === "motd") {
            title = getString("entries.motd").replace("{0}", substitution.date);
            details = substitution.texts.map((text, index) => <span className="detail" key={index}>{text}</span>)
        } else if(substitution.type === "custom") {
            title = substitution.title;
            details = substitution.text;
            hideButton = !substitution.buttonText || substitution.buttonText === "";
            buttonText = hideButton ? "" : substitution.buttonText;
        } else {
            title = "Unknown entry type";
        }
        var shareButtonClass = hideButton ? "shareButtonContainer hide" : "shareButtonContainer";
        return (
            <div className={this.props.data.expanded ? "substitution expanded" : "substitution"} onClick={this.expandToggle}>
                <span className="substitutionTitle">{title}</span>
                <div className="substitutionDescription">{details}</div>
                <div className={shareButtonClass}>
                    <Button passive={true} text={buttonText} onClick={buttonClick} />
                </div>
            </div>
        );
    }
}