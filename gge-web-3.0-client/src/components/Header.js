import React from 'react';
import "../styles/Header.css";
import { hideSettings, showSettings, hideClassChooser } from "../action/otherActions";
import { fetchSubstitutionsStart, fetchSubstitutions } from '../action/substitutionActions';
import { getString } from '../others/language';
import { isAvailable as nativeAvailable, getAppName } from '../others/nativeInterface';


export default class Header extends React.Component {

    constructor(props) {
      super(props);

      this.menuClick = this.menuClick.bind(this);
    }

    menuClick() {
      if(this.props.view === "settings") {
        if(this.props.other.classSelectorState.open) {
          this.props.dispatch(hideClassChooser());
        } else {
          this.props.dispatch(fetchSubstitutionsStart());
          this.props.dispatch(hideSettings());
          this.props.dispatch(fetchSubstitutions());
        }
      } else {
        this.props.dispatch(showSettings());
      }
    }

    render() {
      var icon;
      if(this.props.displaySettingsIcon) {
        icon = this.props.view === "substitutions" ? "settings" : "close";
      }

      var title;

      var appName = getString("app.name");
      
      if(nativeAvailable()) {
        appName = getAppName();
      }

      switch(this.props.view) {
        case "settings":
          title = getString("settings.title");
          break;
        case "login":
          title = getString("login.title");
          break;
        default:
          title = appName;
          break;
      }

      return (
        <div className="header">
            <span className="titleText">{title}</span>
            <div className="titleMenu" onClick={this.menuClick}>
                <i className="material-icons">{icon}</i>
            </div>
        </div>
      );
    }
  }