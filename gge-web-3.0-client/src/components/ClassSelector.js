import React from "react";
import Button from "./Button";
import { fetchClasslistStart, fetchClasslist, hideClassChooser } from "../action/otherActions";
import ClassSelectorElement from "./ClassSelectorElement";
import "../styles/ClassSelector.css";
import { getString } from "../others/language";
import ErrorView from "./ErrorView";

export default class ClassSelector extends React.Component {

    constructor(props) {
        super(props);

        this.retryClassListLoading = this.retryClassListLoading.bind(this);
        this.closeClassChooser = this.closeClassChooser.bind(this);
    }

    componentDidMount() {
        if(!this.props.other.classList.fetching && !this.props.other.classList.loaded) {
            this.props.dispatch(fetchClasslistStart());
            this.props.dispatch(fetchClasslist());
        }
    }

    closeClassChooser() {
        this.props.dispatch(hideClassChooser());
    }

    retryClassListLoading() {
        this.props.dispatch(fetchClasslistStart());
        this.props.dispatch(fetchClasslist());
    }

    render() {
        if(this.props.other.classList.fetching) {
            return <ErrorView dispatch={this.props.dispatch} fetching loaded={false} />
        } else if(this.props.other.classList.error) {
            return (
                <div className="classSelector">
                    <span className="">{this.props.other.classList.error}</span>
                    <Button onClick={this.retryClassListLoading} text={getString("settings.classSwitcher.retry")} />
                    <Button onClick={this.closeClassChooser} text={getString("settings.classSwitcher.back")} />
                </div>
            );
        } else {
            var allClasses = this.props.other.classList.list;
            var enabledClasses = this.props.settings.enabledClasses;
            var enableElements = allClasses.map(class_ => {
                return <ClassSelectorElement enabled={enabledClasses.includes(class_)} key={class_} class_={class_} dispatch={this.props.dispatch} />
            });
            return (
                <div className="classSelector">
                    <div className="classSelectorButtonWrapper">
                        <Button onClick={this.closeClassChooser} text={getString("settings.classSwitcher.back")} />
                    </div>
                    <div className="classSelectorInner">
                        {enableElements}
                    </div>
                    <div className="classSelectorButtonWrapper">
                        <Button onClick={this.closeClassChooser} text={getString("settings.classSwitcher.back")} />
                    </div>
                </div>
            );
        }
    }
}