import React from "react";
import "../styles/Login.css";
import { getString } from "../others/language";
import { startValidateToken, validateToken, hideOtherView } from "../action/otherActions";
import { genCredentailToken } from "../others/authorization";
import { replaceToken, saveSettings } from "../action/settingsActions";
import { fetchSubstitutionsStart, fetchSubstitutions } from "../action/substitutionActions";

export default class Login extends React.Component {

    constructor(props) {
        super(props);

        this.loginSubmit = this.loginSubmit.bind(this);
    }

    loginSubmit(e) {
        var event = e.nativeEvent;
        var username = event.target[0].value;
        var password = event.target[1].value;

        genCredentailToken(username, password).then(token => {
            this.props.dispatch(startValidateToken());
            this.props.dispatch(validateToken(token));
        });

        e.preventDefault();
    }
    
    componentDidUpdate() {
        if(this.props.other.tokenValidationResult) {
            this.props.dispatch(replaceToken(this.props.other.tokenValidationToken));
            this.props.dispatch(hideOtherView());
            this.props.dispatch(saveSettings());
            this.props.dispatch(fetchSubstitutionsStart());
            this.props.dispatch(fetchSubstitutions());
        }
    }

    render() {
        var loginMessage = null;
        if(this.props.other.tokenValidationFetching) {
            loginMessage = getString("login.ongoing");
        } else if(this.props.other.tokenValidationError) {
            loginMessage = getString("login.error");
        } else if(this.props.other.tokenValidationResult === false) {
            loginMessage = getString("login.invalid");
        }
        return (
            <div className="login">
                <span className="loginTitle">{getString("login.title")}</span>
                <form className="loginForm" onSubmit={this.loginSubmit}>
                    <input className="loginUsername" type="text" placeholder={getString("login.username")} label={getString("login.username")}/>
                    <input className="loginPassword" type="password" placeholder={getString("login.password")} label={getString("login.password")} />
                    <input className="loginSubmit" type="submit" value={getString("login.submit")} />
                </form>
                <span className="loginMessage">{loginMessage}</span>
            </div>
        );
    }
}