import { createStore, applyMiddleware } from "redux";
import reducer from "./reducers";
import thunk from 'redux-thunk';

var middleware = applyMiddleware(thunk);

const store = createStore(reducer, middleware);
store.subscribe(() => {
    if(window["__LOG_STORE_CHANGES"]) {
        console.log(store.getState())
    }
});

export default store;