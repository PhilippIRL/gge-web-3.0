import { combineReducers } from "redux";

import settings from "./settingsReducer";
import substitutions from "./substitutionsReducer";
import other from "./otherReducer";

export default combineReducers({settings, substitutions, other});