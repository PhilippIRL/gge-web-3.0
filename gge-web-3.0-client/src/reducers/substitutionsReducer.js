export default function reducer(state={
    substitutions: [],
    fetching: false,
    loaded: false,
    error: null,
    tab: 0
}, action) {
    switch(action.type) {
        case "FETCH_SUBSTITUTIONS_START":
            return {...state, fetching: true, loaded: false, error: null};
        case "FETCH_SUBSTITUTIONS_RESOLVED":
            var internalSubstitutionsArray = action.payload.map(oldObject => {
                return {data: {...oldObject}, expanded: false};
            });
            return {...state, fetching: false, loaded: true, error: null, substitutions: internalSubstitutionsArray};
        case "FETCH_SUBSTITUTIONS_REJECTED":
            return {...state, fetching: false, loaded: true, error: "Unable to fetch substitutions."};
        case "TOGGLE_ENTRY_EXPANDED":
            var entries = [...(state.substitutions)];
            var newEntry = {...entries[action.payload], expanded: !entries[action.payload].expanded};
            entries[action.payload] = newEntry;
            return {...state, substitutions: entries};
        case "SWITCH_TAB":
            return {...state, tab: action.payload};
        default:
            return state;
    }
}