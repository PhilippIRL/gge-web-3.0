export default function reducer(state={
    enabledClasses: [],
    theme: "light",
    authToken: null,
}, action) {
    switch(action.type) {
        case "REPLACE_SETTINGS":
            return {...action.payload};
        case "SET_THEME":
            return {...state, theme: action.payload};
        case "ADD_CLASS":
            // eslint-disable-next-line no-redeclare
            var enabledClasses = [...state.enabledClasses, action.payload];
            return {...state, enabledClasses};
        case "REMOVE_CLASS":
            // eslint-disable-next-line no-redeclare
            var enabledClasses = [...state.enabledClasses];
            enabledClasses.splice(enabledClasses.indexOf(action.payload), 1);
            return {...state, enabledClasses};
        case "REPLACE_TOKEN":
            return {...state, authToken: action.payload};
        default:
            return state;
    }
}