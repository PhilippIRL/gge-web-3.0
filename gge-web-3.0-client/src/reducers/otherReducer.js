export default function reducer(state={
    classList: {
        list: [],
        fetching: false,
        loaded: false,
        error: null,
    },
    view: "substitutions",
    classSelectorState: {
        open: false
    },
    tokenValidationResult: null,
    tokenValidationFetching: false,
    tokenValidationError: null,
    tokenValidationLoaded: false,
    tokenValidationToken: null,
}, action) {
    switch(action.type) {
        case "SHOW_SETTINGS":
            return {...state, view: "settings"};
        case "SHOW_LOGIN":
            return {...state, view: "login"};
        case "HIDE_OTHER_VIEW":
            return {...state, view: "substitutions"};
        case "START_VALIDATE_TOKEN":
            return {...state, tokenValidationError: null, tokenValidationFetching: true, tokenValidationLoaded: false, tokenValidationResult: null, tokenValidationToken: null};
        case "VALIDATE_TOKEN_RESOLVED":
            return {...state, tokenValidationError: null, tokenValidationFetching: false, tokenValidationLoaded: true, tokenValidationResult: action.payload.result, tokenValidationToken: action.payload.token};
        case "VALIDATE_TOKEN_REJECTED":
            return {...state, tokenValidationError: action.payload.error, tokenValidationFetching: false, tokenValidationLoaded: true, tokenValidationResult: null, tokenValidationToken: action.payload.token};
        case "SHOW_CLASS_SELECTOR":
            // eslint-disable-next-line no-redeclare
            var classSelectorState = {...state.classSelectorState, open: true};
            return {...state, classSelectorState};
        case "HIDE_CLASS_SELECTOR":
            // eslint-disable-next-line no-redeclare
            var classSelectorState = {...state.classSelectorState, open: false};
            return {...state, classSelectorState};
        case "FETCH_CLASSLIST_START":
            // eslint-disable-next-line no-redeclare
            var classList = {...state.classList, list: [], fetching: true, loaded: false, error: null};
            return {...state, classList};
        case "FETCH_CLASSLIST_RESOLVED":
            // eslint-disable-next-line no-redeclare
            var classList = {...state.classList, list: action.payload, fetching: false, loaded: true, error: null};
            return {...state, classList};
        case "FETCH_CLASSLIST_REJECTED":
            // eslint-disable-next-line no-redeclare
            var classList = {...state, list: [], fetching: false, loaded: true, error: "Unable to fetch class list"};
            return {...state, classList};
        default:
            return state;
    }
}