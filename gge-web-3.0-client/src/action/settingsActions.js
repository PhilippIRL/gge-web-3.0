import store from "../store";

export function loadSettings() {
    var settingsJSON = window.localStorage.ggeWebSettings;
    var settings = settingsJSON ? JSON.parse(settingsJSON) : null;
    if(settings) {
        return {
            type: "REPLACE_SETTINGS",
            payload: settings,
        }
    } else {
        return {
            type: "NO_SETTINGS_DATA_FOUND",
            payload: null,
        }
    }
}

export function saveSettings() {
    var settings = store.getState().settings;
    var settingsJSON = JSON.stringify(settings);
    window.localStorage.ggeWebSettings = settingsJSON;
    return {
        type: "SETTINGS_SAVED",
        payload: null,
    };
}

export function setTheme(theme) {
    return {
        type: "SET_THEME",
        payload: theme,
    };
}

export function addClass(class_) {
    return {
        type: "ADD_CLASS",
        payload: class_,
    };
}

export function removeClass(class_) {
    return {
        type: "REMOVE_CLASS",
        payload: class_,
    };
}

export function replaceToken(token) {
    return {
        type: "REPLACE_TOKEN",
        payload: token,
    }
}

export default { loadSettings, saveSettings };