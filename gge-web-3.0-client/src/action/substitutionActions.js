import store from "../store";
import { API_BASE } from "../config";
import { addTips } from "../others/tips";
import { replaceToken, saveSettings } from "./settingsActions";
import { writeCache, readCache } from "../others/cache";

export function fetchSubstitutionsStart() {
    return {
        type: "FETCH_SUBSTITUTIONS_START",
        payload: null,
    }
}

export function fetchSubstitutions() {
    return function(dispatch) {
        var enabledClasses = store.getState().settings.enabledClasses;
        var suffix = "";
        if(enabledClasses && enabledClasses.length !== 0) {
            suffix = "?classes=" + encodeURIComponent(enabledClasses.join(","));
        }
        var authToken = store.getState().settings.authToken;
        if(!authToken) {
            dispatch({
                type: "FETCH_SUBSTITUTIONS_REJECTED",
                payload: new Error("Auth token missing"),
            });
            return;
        }
        fetch(API_BASE + "/listSubstitutions" + suffix, {headers: {authorization: "Bearer " + authToken}})
        .then(res => res.json())
        .then(json => {
            if(json.resetToken) {
                store.dispatch(replaceToken(null));
                store.dispatch(saveSettings());
                dispatch({
                    type: "FETCH_SUBSTITUTIONS_REJECTED",
                    payload: new Error("Auth token reset"),
                });
            } else {
                writeCache(json);
                dispatch({
                    type: "FETCH_SUBSTITUTIONS_RESOLVED",
                    payload: addTips(json),
                });
            }
        })
        .catch(err => {
            var cache = readCache();
            if(cache) {
                dispatch({
                    type: "FETCH_SUBSTITUTIONS_RESOLVED",
                    payload: addTips(cache),
                });
            } else {
                dispatch({
                    type: "FETCH_SUBSTITUTIONS_REJECTED",
                    payload: err,
                });
            }
        });
    }
}

export function toggleExpanded(entryIndex) {
    return {
        type: "TOGGLE_ENTRY_EXPANDED",
        payload: entryIndex,
    };
}

export function setTab(tabIndex) {
    return {
        type: "SWITCH_TAB",
        payload: tabIndex,
    };
}

export default { fetchSubstitutionsStart, fetchSubstitutions, toggleExpanded };