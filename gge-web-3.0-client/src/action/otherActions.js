import { API_BASE } from "../config";
import store from "../store";
import { writeClassListCache, readClassListCache } from "../others/cache";

export function showSettings() {
    return {
        type: "SHOW_SETTINGS",
        payload: null,
    };
}

export function showLogin() {
    return {
        type: "SHOW_LOGIN",
        payload: null,
    };
}

export function startValidateToken() {
    return {
        type: "START_VALIDATE_TOKEN",
        payload: null,
    };
}

export function validateToken(token) {
    return function(dispatch) {
        fetch(API_BASE + "/validateToken", {method: "POST", headers: {"authorization": "Bearer " + token}})
        .then(res => res.json())
        .then(json => {
            dispatch({
                type: "VALIDATE_TOKEN_RESOLVED",
                payload: {result: Boolean(json.success), token},
            });
        })
        .catch(err => {
            dispatch({
                type: "VALIDATE_TOKEN_REJECTED",
                payload: {error: err, token},
            });
        });
    }
}

export function hideOtherView() {
    return {
        type: "HIDE_OTHER_VIEW",
        payload: null,
    };
}

export function hideSettings() {
    return hideOtherView();
}

export function showClassChooser() {
    return {
        type: "SHOW_CLASS_SELECTOR",
        payload: null,
    };
}

export function hideClassChooser() {
    return {
        type: "HIDE_CLASS_SELECTOR",
        payload: null,
    };
}

export function fetchClasslistStart() {
    return {
        type: "FETCH_CLASSLIST_START",
        payload: null,
    }
}

export function fetchClasslist() {
    return function(dispatch) {
        var authToken = store.getState().settings.authToken;
        fetch(API_BASE + "/availableClasses", {headers: {authorization: "Bearer " + authToken}})
        .then(res => res.json())
        .then(json => {
            writeClassListCache(json);
            dispatch({
                type: "FETCH_CLASSLIST_RESOLVED",
                payload: json,
            });
        })
        .catch(err => {
            var cache = readClassListCache();
            if(cache) {
                dispatch({
                    type: "FETCH_CLASSLIST_RESOLVED",
                    payload: cache.classList,
                });
            } else {
                dispatch({
                    type: "FETCH_CLASSLIST_REJECTED",
                    payload: err,
                });
            }
        });
    }
}

export default { showSettings, hideSettings, showClassChooser, fetchClasslistStart, fetchClasslist };