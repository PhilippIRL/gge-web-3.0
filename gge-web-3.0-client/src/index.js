import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './components/App';
import { Provider } from "react-redux";
import store from "./store";
import { loadSettings } from './action/settingsActions';
import * as serviceWorker from "./serviceWorker";
import { registerInstallListeners } from "./others/pwa";
import { registerBackListener } from './others/nativeInterface';

store.dispatch(loadSettings());

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

serviceWorker.register();
registerInstallListeners();
registerBackListener();