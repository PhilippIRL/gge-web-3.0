export var API_BASE = "https://ppluss.de/gge/api/v1";

if(window.origin === "http://localhost:3000") {
    API_BASE = "http://localhost:8080/api/v1";
}

export const APP_VERSION = "0.1.2";