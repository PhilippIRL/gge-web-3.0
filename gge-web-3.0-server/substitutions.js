const parser = require("./parser.js");
const { JSDOM } = require("jsdom");
const fetch = require("node-fetch");
const moment = require("moment");
const crypto = require("crypto");
const credentials = require("./credentials.js");

var cache = {data: null, expires: 0};
var availableClasses = ["01SE","5A","5B","5C","5D","6A","6B","6C","7A","7B","7C","8A","8B","8C","9A","9B","10GO","10GR","11GO","11GR","12GO","12GR","BT10_","BT11","BT12","BT13","Goe","KOO","SL_"];

function getURLForWeek(week) {
    var weekStr = week.toString();
    if(weekStr.length === 1) {
        weekStr = "0" + weekStr;
    }
    return `https://www.grashof-gymnasium-bredeney.de/vp/VPonline/${weekStr}/w/w00000.htm`;
}

function getURLs() {
    var urls = [];
    var week = moment().week();
    var nextWeek = week + 1;
    var dayOfWeek = moment().day();
    switch(dayOfWeek) {
        case 0: // Sunday
            urls.push({url: getURLForWeek(week), tolerateFail: false});
            break;
        case 1:
        case 2:
        case 3:
        case 4:
            urls.push({url: getURLForWeek(week), tolerateFail: false});
            break;
        case 5:
            urls.push({url: getURLForWeek(week), tolerateFail: false});
            urls.push({url: getURLForWeek(nextWeek), tolerateFail: true});
            break;
        case 6:
            urls.push({url: getURLForWeek(nextWeek), tolerateFail: false});
            break;
    }
    return urls;
}

function getAuthString() {
    return Buffer.from(credentials.username + ":" + credentials.password).toString("base64");
}

async function fetchPage(urlData) {
    try {
        var result = await fetch(urlData.url, {
            headers: {
                "authorization": "Basic " + getAuthString(),
            }
        });
        var buffer = await result.arrayBuffer();
        var decoder = new TextDecoder("ISO-8859-1");
        var text = decoder.decode(buffer);
        return text;
    } catch (error) {
        if(!urlData.tolerateFail) {
            throw error;
        }
        return null;
    }
}

async function getSubstitutions() {
    var urls = getURLs();
    var promises = urls.map(url => fetchPage(url));
    var pages = await Promise.all(promises);
    var filteredPages = pages.filter(page => {
        if(page === null) {
            return false;
        }
        return true;
    });
    var results = filteredPages.map(page => {
        var jsdom = new JSDOM(page);
        return parser(jsdom.window.document);
    });
    var filteredResults = results.filter(result => {
        var daysSinceSchoolYearStart = moment(result.schoolYearStart, "DD.MM.YYYY").diff(moment(), "days") * -1;
        // If the school half-year is older than 200 days it's probably already over, because a school half-year only takes half a year, so the substitutions are probably from last year
        if(daysSinceSchoolYearStart > 200) {
            return false;
        }
        return true;
    });
    var substitutionsArray = [];
    filteredResults.forEach(resultPage => {
        resultPage.entries.forEach(entry => substitutionsArray.push(entry));
    });
    return substitutionsArray;
}

async function getSubstitutionCache() {
    if(Date.now() > cache.expires) {
        var newData = await getSubstitutions();
        cache.data = newData;
        cache.expires = Number(moment().add(1, "minute").format("x"));
        return newData;
    } else {
        return cache.data;
    }
}

function filterSubstitutions(substitutions, classes) {
    return substitutions.filter(substitution => {
        if(substitution.type === "substitution") {
            var include = false;
            for(var i = 0; i < classes.length; i++) {
                var currentClass = classes[i];
                
                if(substitution.class_.toUpperCase().includes(currentClass.toUpperCase())) {
                    include = true;
                }

                if(substitution.class_.toUpperCase() === currentClass.substring(0, 1).toUpperCase()) {
                    include = true;
                }

                if(currentClass.length === 2 &&
                    substitution.class_.toUpperCase().includes(currentClass.substring(0, 1).toUpperCase()) &&
                    substitution.class_.toUpperCase().includes(currentClass.substring(1, 2).toUpperCase())) {
                    include = true;
                }

                if(include) break;
            }
            return include;
        }
        return true;
    });
}

async function getSubstitutionsForClasses(classList) {
    var substitutions = await getSubstitutionCache();
    var filteredSubstitutions = filterSubstitutions(substitutions, classList);
    return filteredSubstitutions;
}

var validTokenHashes = [
    "694b355164ce7cc48a2618ac9aabd8496c8e252a0457e0b0cc0d4467f15533d698b75d647d1567f3fca8c250d71778dc9e9382fad1ab1df727f414b1817fbc65",
    "1d935173f96d255af2f846d3cc169e71094cb390cec64916a376f375641ba72855614d1140c3073d083e8893d2955c1bd8340fd2cd1a7484a2584df581e9f838",
];

function validateToken(token) {
    var tokenHash = crypto.createHash("sha512").update(token).digest("hex").toString();
    return validTokenHashes.includes(tokenHash);
}

module.exports = {getSubstitutionsForClasses, getSubstitutions: getSubstitutionCache, getClassList: () => availableClasses, validateToken};