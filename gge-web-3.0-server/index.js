const {getSubstitutionsForClasses, getSubstitutions, getClassList, validateToken} = require("./substitutions.js");
const express = require("express");

var app = express();

app.options("/api/v1/validateToken", async (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "authorization");
    res.send();
})

app.post("/api/v1/validateToken", async (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "authorization");
    if(req.header("authorization")) {
        var authHeader = req.header("authorization");
        var authToken = authHeader.split(" ")[1];
        if(authToken.length === 40 && validateToken(authToken)) {
            res.send({success: true});
            return;
        } 
    }
    res.send({success: false});
});

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "authorization");
    if(req.header("authorization")) {
        var authHeader = req.header("authorization");
        var authToken = authHeader.split(" ")[1];
        if(authToken.length === 40 && validateToken(authToken)) {
            next();
            return;
        } 
    }
    //res.status(401);
    res.send({error: "Invalid or missing authorization data", resetToken: true});
});

app.get("/api/v1/availableClasses", async (req, res) => {
    res.send(getClassList());
});

app.get("/api/v1/listSubstitutions", async (req, res) => {
    try {
        if(req.query.classes) {
            var classes = req.query.classes.split(",");
            if(classes.length > 50) {
                res.status(400);
                res.send({error: "You can only request up to 50 filtered classes."});
                return;
            }
            var filteredClasses = classes.filter(className => className.length < 8 && className.length >= 2);
            if(filteredClasses.length === 0) {
                res.status(400);
                res.send({error: "No valid classes were supplied."});
                return;
            }
            res.send(await getSubstitutionsForClasses(filteredClasses));
        } else {
            res.send(await getSubstitutions());
        }
    } catch (error) {
        res.send([{type: "custom", text: "The server is unable to process the data provided by the school-server.", title: "Internal Error", date: "Server Messages"}]);
    }
    
});

app.listen(process.env.PORT || 8080, () => console.log("Listening..."));