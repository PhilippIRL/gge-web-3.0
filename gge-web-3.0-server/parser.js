"use strict";

function getSubstitutions(document) {
  // Aktualität prüfen
  var rawDateString = document.querySelectorAll("body font")[1].textContent.trim();
  var dateStrings = rawDateString.split(" ");
  var dateString = dateStrings[dateStrings.length - 1];

  // Alle Tabellen auf der Seite zusammentragen
  var tables = document.querySelectorAll("tbody");

  var entries = [];

  tables.forEach((elem, index) => {
    // Schonmal alle nicht freigegeben Vertretungstabellen aussortieren
    if(!(elem.textContent === "Vertretungen sind nicht freigegeben")) {
      var rows = elem.querySelectorAll("tr");
      var firstrow = rows[0].children;
      // Wenn es sich um eine Nachricht des Tages handelt...
      if(firstrow.length === 1 && firstrow[0].textContent === "Nachrichten zum Tag") {
        var obj = {type: "motd", texts: []};

        /* Untis speichert das Datum leider nicht in der
        Tabelle zur Nachricht des Tages. Mit diesen komischen
        Befehlen wird versucht das Datum, welches über der
        Tabelle steht, in Erfahrung zu bringen. */
        try {
          var pTag = elem.parentNode.parentNode;
          /* Montags wird die Zeile mit dem Datum nicht in
          einen p-Tag gepackt, also versuche ich hiermit einfach
          das Datum aus dem ersten b-Tag im Vertretungsdiv zu nehmen. */
          if(pTag === pTag.parentElement.querySelectorAll("p")[0]) {
            var bTag = pTag.parentElement.querySelectorAll("b")[0];
          } else {
            var pTags = Array.from(document.querySelectorAll("#vertretung p"));
            var pIndex = pTags.indexOf(pTag);
            var bTag = pTags[pIndex-1].querySelector("b");
          }
          obj.date = bTag.textContent.split(" ")[0];
        } catch(e) {
          console.error("unable to get motd date", e);
        }

        // Alle Elemente der Tabelle (außer die Kopfzeile (deswegen i = 1 und nicht 0)) parsen.
        for(var i = 1; i < rows.length; i++) {
          obj.texts.push(rows[i].textContent);
        }

        entries.push(obj);

      // Wenn es sich um eine "normale" Vertretungstabelle handelt...
      } else if(rows[0].className === "list") {
        // Die Zeilen der Tabelle (wieder ohne Kopfzeile) durchgehen
        for(var i = 1; i < rows.length; i++) {
          var obj = {type: "substitution"};
          var columns = rows[i].children;

          obj.date = columns[0].textContent;
          obj.lesson = columns[1].textContent;
          obj.class_ = columns[2].textContent;
          obj.subject = columns[3].textContent;
          obj.oldroom = columns[4].textContent;
          obj.newroom = columns[5].textContent;
          obj.substitute = columns[6].textContent;
          obj.text = columns[7].textContent;

          entries.push(obj);
        }
      }
    }
  });

  return {entries, schoolYearStart: dateString};
}

module.exports = getSubstitutions;